package com.mindtree.flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FlipkartTest{
	static ChromeDriver wd;

	@Test(priority=1)
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "F:\\Automation\\FlipkartBuild\\driver\\chromedriver.exe");
		wd = new ChromeDriver();
		wd.get("https://www.flipkart.com");
	}
	@Test(priority=2)
	public void login()
	{
		wd.findElement(By.xpath("//input[@class='_2IX_2- VJZDxU']")).sendKeys("9741594929");
		wd.findElement(By.xpath("//input[@class='_2IX_2- _3mctLh VJZDxU']")).sendKeys("Smitha@1998");
		wd.findElement(By.xpath("//button[@class='_2KpZ6l _2HKlqd _3AWRsL']")).click();
		System.out.print("Completed Test1");
	}
}
